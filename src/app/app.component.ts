import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MessageService } from '../shared/message.service';
import { Message } from '../shared/message.model';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  messageChangeSubscription!: Subscription;
  messageIsFetchingSubscription!: Subscription;
  isFetching = false;
  @ViewChild('f') messageForm!: NgForm;
  isUploading = false;
  messageUploadingSubscription!: Subscription;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.messageService.start();

    this.messageChangeSubscription = this.messageService.messageChange.subscribe(message => {
      this.messages = message;
    });
    this.messageIsFetchingSubscription = this.messageService.messageFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.messageUploadingSubscription = this.messageService.messageUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }


  sendMessage() {
    const newMessage = new Message('', this.messageForm.value.message, this.messageForm.value.author, '');
    this.messageForm.form.setValue({message: '', author: ''});
    this.messageService.postMessage(newMessage).subscribe();
  }

  ngOnDestroy(): void {
    this.messageService.stop();
    this.messageChangeSubscription.unsubscribe();
    this.messageIsFetchingSubscription.unsubscribe();
    this.messageUploadingSubscription.unsubscribe();
  }
}
