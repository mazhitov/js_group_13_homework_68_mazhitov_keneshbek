import { Injectable } from '@angular/core';
import { Message } from './message.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Observable, Subject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messageFetching = new Subject<boolean>();
  messageUploading = new Subject<boolean>();
  messageChange = new Subject<Message[]>();
  private messages: Message[] = [];
  messagesSubscription!: Subscription;
  lastDate = '';
  url = 'http://146.185.154.90:8000/messages?datetime=';

  constructor(private http: HttpClient) {
  }

  start() {
    const fetchObservable = new Observable<Message[]>(subscriber => {

      const interval = setInterval(() => {
        if (!this.lastDate) this.messageFetching.next(true);

        this.http.get<Message[]>(this.url + this.lastDate)
          .pipe(map(result => {
            if (result === null) {
              return [];
            }
            if (result.length !== 0) this.messageFetching.next(true);
            return result.map(item => {
              return new Message(item._id, item.message, item.author, item.datetime);
            });
          }))
          .subscribe(messages => {
            this.messages.push(...messages);
            if (this.messages.length > 0) {
              this.lastDate = this.messages[this.messages.length - 1]['datetime'];
              subscriber.next();
            }
          }, () => {
            this.messageFetching.next(false);
          });
      }, 3000);

      return {
        unsubscribe() {
          clearInterval(interval);
        }
      }
    });

    this.messagesSubscription = fetchObservable.subscribe(() => {
      this.messageChange.next(this.messages.slice());
      this.messageFetching.next(false);
    }, error => {
      console.log(error.message);
    })
  }

  stop() {
    this.messagesSubscription.unsubscribe();
  }

  postMessage(message: Message) {
    this.messageUploading.next(true);
    const body = new HttpParams()
      .set('author', message.author)
      .set('message', message.message);
    return this.http.post(this.url, body)
      .pipe(tap(() => {
          this.messageUploading.next(false);
        }, () => {
          this.messageUploading.next(false);
        })
      );

  }
}
